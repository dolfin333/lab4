require('dotenv').config();

const config = {
  app: {
    port: parseInt(process.env.PORT) || 5000
  },
  db: {
    dbConnectionString: process.env.DB_CONNECTION_STRING,
    dbConnectionStringTest: process.env.DB_CONNECTION_STRING_TEST
  },
  cloudinary: {
    cloud_name: process.env.CLOUD_NAME,
    api_key: process.env.API_KEY,
    api_secret: process.env.API_SECRET
  }
};

module.exports = config;
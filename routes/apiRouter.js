const apiRouter = require('express').Router();
const userRouter = require('./users.js');
const courseRouter = require('./courses.js');
const scoreRouter = require('./scores.js');

apiRouter.use('/users', userRouter);
apiRouter.use('/courses', courseRouter);
apiRouter.use('/scores', scoreRouter);

module.exports = apiRouter;
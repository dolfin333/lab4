const scoreRouter = require('express').Router();
const scoreController = require('../controllers/scoreController');

scoreRouter
    .get('/new', scoreController.newScore)
    .get('/:id/update', scoreController.getAndUpdateScore)
    .post('/:id/update', scoreController.updateScore)
    .get('/:id/updatePhoto', scoreController.getAndUpdateScorePhoto)
    .post('/:id/updatePhoto', scoreController.updateScorePhoto)
    .get('/:id', scoreController.getScoreById)
    .post('/:id', scoreController.deleteScoreById)
    .get('/', scoreController.getScores)
    .post('/', scoreController.addScore);

module.exports = scoreRouter;

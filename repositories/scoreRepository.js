const Score = require('../models/score.js');

class ScoreRepository {

    async getScores() {
        const scores = await Score.find().populate('course_id').populate('user_id');
        return scores;
    }

    async getScoreById(id) {
        const score = await Score.findById(id).populate('course_id').populate('user_id');
        return score;
    }

    async addScore(scoreModel) {
        const score = new Score(scoreModel);
        const newScore = await score.save();
        return newScore.id;
    }

    async updateScore(id, updatedInfo) {
        const updatedScore = await Score.updateOne({ _id: id }, {
            $set: {
                user_id: updatedInfo.user_id,
                course_id: updatedInfo.course_id,
                score: updatedInfo.score,
                ScoreDate: updatedInfo.ScoreDate
            }
        });
        return updatedScore;
    }

    async updateScorePhoto(id, img) {
        const updatedScore = await Score.updateOne({ _id: id }, {
            $set: {
                img: img
            }
        });
        return updatedScore;
    }

    async deleteScore(id) {
        const deletedScore = await Score.deleteOne({ _id: id });
        return deletedScore;
    }
}

module.exports = ScoreRepository;
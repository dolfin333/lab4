/**
 * @typedef User
 * @property {integer} id
 * @property {string} login.required
 * @property {string} fullname
 * @property {integer} role
 * @property {date} registeredAt
 * @property {string} avaUrl
 * @property {bool} isEnabled
 */
 const mongoose = require('mongoose');

 const UserSchema = new mongoose.Schema({
     login: { type: String, required: true },
     fullname: String,
     role: Number,
     registeredAt: { type: Date },
     avaUrl: Array,
     isEnabled: Boolean,
     biography: String
 });
 
 const User = mongoose.model('users', UserSchema);
 
 module.exports = User;
 
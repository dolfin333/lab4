/**
 * @typedef Course
 * @property {integer} id
 * @property {string} name
 * @property {integer} duration
 * @property {string} group
 * @property {integer} credits
 * @property {string} date
 */


const mongoose = require('mongoose');

const CoursesSchema = new mongoose.Schema({
    name: { type: String, required: true },
    duration: Number,
    group: String,
    credits: Number,
    date: { type: Date },
    img: String
});

const Course = mongoose.model('courses', CoursesSchema);

module.exports = Course;
const express = require('express');
const consolidate = require('consolidate');
const mustache_ex = require('mustache-express');
const path = require('path');
const config = require('./config');
const app = express();
const mongoose = require('mongoose');
const connectOptions = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
};

const morgan = require('morgan');
const body_parser = require('body-parser');
const busboy_body_parser = require('busboy-body-parser');
const apiRouter = require('./routes/apiRouter');
const { mustache } = require('consolidate');

app.use(body_parser.urlencoded({ extended: true }))
app.use(body_parser.json());
app.use(busboy_body_parser());

app.use((err, req, res, next) => {
    if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
        res.status(400).send({ mess: "Bad request" });
        return;
    }

    next();
});



const viewsDir = path.join(__dirname, 'views');
app.engine("mst", mustache_ex(path.join(viewsDir, "partials")));
app.set('views', viewsDir);
app.set('view engine', 'mst');
// usage
app.get('/', function (req, res) {
    res.render('index', { index_current: 'current', home_link: 'disabled_link' });
});


app.get('/about', function (req, res) {
    res.render('about', { about_current: 'current', about_link: 'disabled_link' });
});


app.use('', apiRouter);
app.use(express.static("./public"));
app.use(express.static("./data"));
app.use(morgan('dev'));



app.listen(config.app.port || 3000, async () => {
    try {
        console.log(`Server ready`);
        const client = await mongoose.connect(config.db.dbConnectionString || config.db.dbConnectionStringTest, connectOptions);
        console.log('Mongo database connected');
    } catch (err) {
        console.log(err);
        console.log('Mongo database NOT connected');
    }
});